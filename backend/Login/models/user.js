const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/db');

// User Schema
const ProfileSchema = mongoose.Schema({

  username: {
    type: String,
    required: true,
    unique:true
  },
  first_name: {
    type: String,
    required: true,
    
  },
  last_name: {
    type: String,
    required: true,
    
  },
  password: {
    type: String,
    required: true,
    
  },
gender:{
    type:String,
    required:true
},
  cpassword:
 {
    type:String,
  required:true
 }
    
});

const User = module.exports = mongoose.model('UsersRegistration', ProfileSchema);

module.exports.getUserById = function(id, callback){
  User.findById(id, callback);
}

module.exports.getUserByUserName = function(username, callback){
  const query = {username: username}
  User.findOne(query, callback);

}

module.exports.addUser = function(newUser, callback){
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(newUser.password, salt, (err, hash) => {
      if(err) throw err;
      newUser.password = hash;
      newUser.save(callback);

});
  });
}

module.exports.comparePassword = function(candidatePassword, hash, callback){
  bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
    if(err) throw err;
    callback(null, isMatch);
  });
}
