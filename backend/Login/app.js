const express=require('express')
const path=require('path')
const bodyParser=require('body-parser')
const cors=require('cors')
const mongoose=require('mongoose')
const passport=require('passport')
const config=require('./config/db')

const jwt = require('jsonwebtoken');

mongoose.connect(config.database,{useNewUrlParser:true,useUnifiedTopology:true}).then(
    (res)=>{
        console.log('database connected');
    }
).catch((err)=>{
    console.log('database not connected');
})
const app=express()
app.use(cors())
const users=require('./routes/users')
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

const port=3000
app.use('/users',users)
app.use(passport.initialize())
app.use(passport.session())
require('./config/passport')(passport)
app.get('/',(req,res)=>{
    res.send('<h1>Invalid EndPoint</h1>')
})

app.listen(port,()=>{
    console.log('Server up and running on '+port);
})
