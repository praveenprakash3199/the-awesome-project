import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './component/welcome/welcome.component';
import { AuthGuard } from './guards/auth.guard';
import { LoginFormComponent } from './component/login-form/login-form.component';
import { RegisterFormComponent } from './component/register-form/register-form.component';

const routes: Routes = [

{ path: "", redirectTo: "login-form", pathMatch: "full" },
{path:'login-form',component:LoginFormComponent},
{path:'register-form',component:RegisterFormComponent},

{path:'welcome',component:WelcomeComponent,},




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents=[
  LoginFormComponent,
  WelcomeComponent,
  RegisterFormComponent,

]


