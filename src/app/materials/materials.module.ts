import {NgModule} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import {MatTabsModule} from '@angular/material/tabs';
import {MatRadioModule} from '@angular/material/radio';
const bookStoreMaterials=[
   MatButtonModule,
   MatTableModule,
   MatSelectModule,
   MatPaginatorModule,
   MatInputModule,
   MatTabsModule,
   MatRadioModule

]

@NgModule({
  imports:[bookStoreMaterials],
  exports:[bookStoreMaterials]
})

export class MaterialsModule{}
