import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  userName= new FormControl('',[Validators.required]);

  password= new FormControl('',[Validators.required]);



  registerForm: FormGroup;

  constructor(private fb : FormBuilder, private router: Router,  private authService: AuthServiceService ) {
    this.registerForm= this.fb.group({
      userName: this.userName,
      password   : this.password,

    })
   }




  ngOnInit(): void {
  }
  submit(){
    console.log(this.registerForm);

    const user = {
      username: this.userName.value,
      password: this.password.value,

    }

    console.log(user);



    this.authService.authenticateUser(user).subscribe(data => {
      if ((data as any).success) {
        this.authService.storeUserData((data as any).token, (data as any).user);
        alert("Successfull");

         this.router.navigate(['welcome']);
      } else {
        alert("Un-Successfull");
         this.router.navigate(['author-menu']);
      }
    });


  }
}
