import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthServiceService } from 'src/app/services/auth-service.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {
  genders: string[] = ['Male', 'Female'];

  userName= new FormControl('',[Validators.required,Validators.pattern('^[a-zA-Z0-9_]{5,20}$')]);
  firstName= new FormControl('',[Validators.required]);
  lastName= new FormControl('',[Validators.required])

  password= new FormControl('',[Validators.required,
    Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[!,@,#,$,%]).{6,}')])

  confirmpassword= new FormControl('',[Validators.required,])

   gender = new FormControl('',[Validators.required] )

  registerForm: FormGroup;

  constructor(private fb : FormBuilder, private authService: AuthServiceService,private router: Router) {
    this.registerForm= this.fb.group({
      userName: this.userName,
      firstName:this.firstName,
      lastName:this.lastName,
      gender: this.gender,
      password   : this.password,
      confirmpassword  :this.confirmpassword,
    },{
      validator: this.MustMatch('password','confirmpassword')
    })
   }


   MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];
    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
    return;
    }
    if (control.value !== matchingControl.value) {
    matchingControl.setErrors({ mustMatch: true });
    } else {

    matchingControl.setErrors(null);
    }
    }
    }

  ngOnInit(): void {



  }
  submit(){
    console.log(this.registerForm);
    const user = {


      username: this.userName.value,
      first_name:this.firstName.value,
      last_name:this.lastName.value,
      gender:this.gender.value,
      password: this.password.value,
      cpassword: this.confirmpassword.value,
    }
console.log(this.userName,this.firstName);

    this.authService.registerUser(user).subscribe(data => {
      if ((data as any).success) {

        this.router.navigate(['/login-form']);
      } else {

        this.router.navigate(['/register-form']);
      }
      console.log(data);

    });
  }
}
